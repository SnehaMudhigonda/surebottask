package com.surebot.myapplication.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.surebot.myapplication.R;
import com.surebot.myapplication.database.DatabaseHelper;
import com.surebot.myapplication.model.User;
import com.surebot.myapplication.utils.SharedPreferenceManager;

import androidx.appcompat.app.AppCompatActivity;

import static com.surebot.myapplication.utils.Util.isValidEmail;

public class LoginActivity extends AppCompatActivity {

    private EditText emailET, passwordET;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button loginButton = findViewById(R.id.loginButton);
        Button sigupButton = findViewById(R.id.registerButton);
        emailET = findViewById(R.id.emailET);
        passwordET = findViewById(R.id.passwordET);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = emailET.getText().toString();
                String password = passwordET.getText().toString();
                if (email.isEmpty()) {
                    showToastMessage("Please enter email Id");
                } else if (!isValidEmail(email)) {
                    showToastMessage("Please enter valid Email Id");
                } else if (password.isEmpty()) {
                    showToastMessage("Please enter password");
                } else {
                    //check from DB
                    DatabaseHelper databaseHelper = new DatabaseHelper(LoginActivity.this);
                    User user = databaseHelper.getUser(email, password);
                    if (user.getId() != null) { // if user exists
                        showToastMessage("Successfully login. Welcome: " + user.getFirstName());
                        emailET.getText().clear();
                        passwordET.getText().clear();
                        SharedPreferenceManager sharedPreferenceManager = new SharedPreferenceManager(LoginActivity.this);
                        sharedPreferenceManager.save("email", email);
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        finish(); // finish the login activity
                    } else { // if user not exists
                        showToastMessage("User not exist or invalid credentials");
                    }
                }
            }
        });

        sigupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), SignUpActivity.class);
                startActivity(i);
            }
        });

    }

    private void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

}
