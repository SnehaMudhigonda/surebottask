package com.surebot.myapplication.activities;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.surebot.myapplication.R;
import com.surebot.myapplication.database.DatabaseHelper;
import com.surebot.myapplication.model.User;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import static com.surebot.myapplication.utils.Util.isValidEmail;

public class SignUpActivity extends AppCompatActivity {

    private EditText firstNameET, lastNameET, dobET, emailET, phoneNumberET, passwordET, confirmPasswordET;

    final Calendar myCalendar = Calendar.getInstance();
    private DatePickerDialog.OnDateSetListener date;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        firstNameET = findViewById(R.id.firstNameET);
        lastNameET = findViewById(R.id.lastNameET);
        dobET = findViewById(R.id.dobET);
        emailET = findViewById(R.id.emailET);
        phoneNumberET = findViewById(R.id.phoneNumberET);
        passwordET = findViewById(R.id.passwordET);
        confirmPasswordET = findViewById(R.id.confirmPasswordET);
        Button registerButton = findViewById(R.id.registerButton);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String firstName = firstNameET.getText().toString();
                String lastName = lastNameET.getText().toString();
                String dob = dobET.getText().toString();
                String email = emailET.getText().toString();
                String phoneNumber = phoneNumberET.getText().toString();
                String password = passwordET.getText().toString();
                String confirmPassword = confirmPasswordET.getText().toString();

                if (firstName.isEmpty()) {
                    showToastMessage("Please enter First Name");
                } else if (lastName.isEmpty()) {
                    showToastMessage("Please enter Last Name");
                } else if (dob.isEmpty()) {
                    showToastMessage("Please enter Date of birth");
                } else if (email.isEmpty()) {
                    showToastMessage("Please enter Email Id");
                } else if (!isValidEmail(email)) {
                    showToastMessage("Please enter valid Email Id");
                } else if (phoneNumber.isEmpty()) {
                    showToastMessage("Please enter Phone Number ");
                } else if (phoneNumber.length() != 10) {
                    showToastMessage("Please enter valid Phone Number ");
                } else if (password.isEmpty()) {
                    showToastMessage("Please enter Password");
                } else if (confirmPassword.isEmpty()) {
                    showToastMessage("Please enter Confirm Password");
                } else if (!confirmPassword.equals(password)) {
                    showToastMessage("Password and confirm Password should be same");
                } else {
                    // Save in DB
                    DatabaseHelper databaseHelper = new DatabaseHelper(SignUpActivity.this);

                    // Create User
                    User user = new User();
                    // Set values
                    user.setFirstName(firstName);
                    user.setLastName(lastName);
                    user.setDob(dob);
                    user.setEmailId(email);
                    user.setPhoneNumber(phoneNumber);
                    user.setPassword(password);

                    //  Save user in DB
                    boolean isSaved = databaseHelper.addUser(user);
                    if (isSaved) {
                        showToastMessage("Successfully registered please login");
                        finish();
                    } else {
                        showToastMessage("Something went wrong while registering");
                    }
                }
            }
        });


        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        dobET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(SignUpActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMaxDate(myCalendar.getTimeInMillis());
                datePickerDialog.show();
            }
        });
    }

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        dobET.setText(sdf.format(myCalendar.getTime()));

    }

    private void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

}
