package com.surebot.myapplication.fragments;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.surebot.myapplication.R;
import com.surebot.myapplication.database.DatabaseHelper;
import com.surebot.myapplication.model.User;
import com.surebot.myapplication.utils.SharedPreferenceManager;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import pub.devrel.easypermissions.EasyPermissions;

public class DashboardFragment extends Fragment implements EasyPermissions.PermissionCallbacks {

    private View myFragmentView;
    private static final String KEY_FILE_PATH = "filePath";
    private static final int PICKFILE_RESULT_CODE = 1;
    private static final int EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE = 2;
    private TextView filePathTV;
    private ImageView profilePicIV;
    private SharedPreferenceManager sharedPreferenceManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myFragmentView = inflater.inflate(R.layout.fragment_dashboard, container, false);
        return myFragmentView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView firstNameTV = myFragmentView.findViewById(R.id.firstNameTV);
        TextView lastNameTV = myFragmentView.findViewById(R.id.lastNameTV);
        TextView dobTV = myFragmentView.findViewById(R.id.dobTV);
        TextView emailIdTV = myFragmentView.findViewById(R.id.emailTV);
        TextView phoneNoTV = myFragmentView.findViewById(R.id.phoneNumberTV);
        filePathTV = myFragmentView.findViewById(R.id.filePathTV);
        Button browseButton = myFragmentView.findViewById(R.id.browseButton);
        profilePicIV = myFragmentView.findViewById(R.id.profilePicIV);

        sharedPreferenceManager = new SharedPreferenceManager(getActivity());
        String email = sharedPreferenceManager.getValueByKey("email");
        String filePath = sharedPreferenceManager.getValueByKey(KEY_FILE_PATH);


        // get user by email from DB
        DatabaseHelper databaseHelper = new DatabaseHelper(getActivity());
        User user = databaseHelper.getUserByEmail(email);

        // Set user data
        firstNameTV.setText(user.getFirstName());
        lastNameTV.setText(user.getLastName());
        dobTV.setText(user.getDob());
        emailIdTV.setText(user.getEmailId());
        phoneNoTV.setText(user.getPhoneNumber());

        if (filePath != null) {
            profilePicIV.setImageURI(Uri.parse(filePath));
            filePathTV.setText(filePath);
        }

        browseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!EasyPermissions.hasPermissions(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    EasyPermissions.requestPermissions(getActivity(), getString(R.string.permission_read_external_storage), EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE, Manifest.permission.READ_EXTERNAL_STORAGE);
                } else {
                    startFileChooserIntent();
                }
            }
        });
    }

    private void startFileChooserIntent() {
        Intent chooseFile = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        chooseFile.setType("image/*");
        chooseFile = Intent.createChooser(chooseFile, "Choose a file");
        startActivityForResult(chooseFile, PICKFILE_RESULT_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICKFILE_RESULT_CODE) {
            if (resultCode == -1) {
                Uri fileUri = data.getData();
                assert fileUri != null;
                String filePath = fileUri.getPath();
                filePathTV.setText(filePath);
                profilePicIV.setImageURI(fileUri);
                sharedPreferenceManager.save(KEY_FILE_PATH, fileUri.toString());
            }
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        startFileChooserIntent();
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        showToastMessage("Please allow permission to choose a file");
    }

    private void showToastMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }
}