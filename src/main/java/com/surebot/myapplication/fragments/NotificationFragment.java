package com.surebot.myapplication.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.surebot.myapplication.R;
import com.surebot.myapplication.adapter.ListNamesAdapter;
import com.surebot.myapplication.model.ListItem;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


public class NotificationFragment extends Fragment {

    private View myFragmentView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myFragmentView = inflater.inflate(R.layout.fragment_notification, container, false);
        loadNames();
        return myFragmentView;
    }

    private void loadNames() {
        List<ListItem> list = new ArrayList<>();
        for (int i = 1; i < 100; i++) {
            ListItem listItem = new ListItem();
            listItem.setName("Sample Name " + i);
            list.add(listItem);
        }

        RecyclerView recyclerView = myFragmentView.findViewById(R.id.recyclerView);
        ListNamesAdapter adapter = new ListNamesAdapter(list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
    }
}
