package com.surebot.myapplication.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.surebot.myapplication.R;
import com.surebot.myapplication.adapter.ImageSliderAdapter;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import me.relex.circleindicator.CircleIndicator;

public class SearchFragment extends Fragment {

    private static ViewPager mPager;
    private static int currentPage = 0;
    private static final Integer[] images = {R.drawable.image_one, R.drawable.image_two, R.drawable.image_three};
    private ArrayList<Integer> imagesArray = new ArrayList<Integer>();
    private View myFragmentView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myFragmentView = inflater.inflate(R.layout.fragment_search, container, false);
        loadImages();
        return myFragmentView;
    }

    private void loadImages() {
        for (int i = 0; i < images.length; i++) {
            imagesArray.add(images[i]);
        }

        mPager = myFragmentView.findViewById(R.id.pager);
        mPager.setAdapter(new ImageSliderAdapter(getActivity(), imagesArray));
        CircleIndicator indicator = myFragmentView.findViewById(R.id.indicator);
        indicator.setViewPager(mPager);

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == images.length) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 2500, 2500);
    }
}
