package com.surebot.myapplication.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.surebot.myapplication.model.User;

import androidx.annotation.Nullable;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String TABLE_USER = "User";
    private static final String KEY_ID = "Id";
    private static final String FIRST_NAME = "FirstName";
    private static final String LAST_NAME = "LastName";
    private static final String DATE_OF_BIRTH = "Dob";
    private static final String EMAIL = "Email";
    private static final String PHONE_NO = "PhoneNo";
    private static final String PASSWORD = "Password";
    private static final String DATABASE_NAME = "SurebotDB";
    private static final int DATABASE_VERSION = 1;

    public DatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_USER_TABLE = "CREATE TABLE " + TABLE_USER + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + FIRST_NAME + " TEXT,"
                + LAST_NAME + " TEXT,"
                + DATE_OF_BIRTH + " TEXT,"
                + EMAIL + " TEXT,"
                + PHONE_NO + " TEXT,"
                + PASSWORD + " TEXT"
                + ")";
        sqLiteDatabase.execSQL(CREATE_USER_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // Drop older table if existed
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        // Create tables again
        onCreate(sqLiteDatabase);
    }

    // code to add the new user
    public boolean addUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(FIRST_NAME, user.getFirstName());
        values.put(LAST_NAME, user.getLastName());
        values.put(DATE_OF_BIRTH, user.getDob());
        values.put(EMAIL, user.getEmailId());
        values.put(PHONE_NO, user.getPhoneNumber());
        values.put(PASSWORD, user.getPassword());

        // Inserting Row
        db.insert(TABLE_USER, null, values);
        //2nd argument is String containing nullColumnHack
        db.close(); // Closing database connection
        return true;
    }

    // code to get the single user
    public User getUser(String email, String password) {
        User user = new User();
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_USER + " WHERE " + EMAIL + " = '" + email + "' AND " + PASSWORD + " = '" + password + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() < 1) {
            cursor.close();
            return user;
        } else {
            cursor.moveToFirst();

            int userId = Integer.parseInt(cursor.getString(0));
            String firstName = cursor.getString(1);
            String lastName = cursor.getString(2);
            String Dob = cursor.getString(3);
            String emailId = cursor.getString(4);
            String phoneNo = cursor.getString(5);
            String newpassword = cursor.getString(6);

            user.setId(userId);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setDob(Dob);
            user.setEmailId(emailId);
            user.setPhoneNumber(phoneNo);
            user.setPassword(newpassword);
        }
        cursor.close();

        // return contact
        return user;
    }

    public User getUserByEmail(String email){
        User user = new User();
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_USER + " WHERE " + EMAIL + " = '" + email + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() < 1) {
            cursor.close();
            return user;
        } else {
            cursor.moveToFirst();

            int userId = Integer.parseInt(cursor.getString(0));
            String firstName = cursor.getString(1);
            String lastName = cursor.getString(2);
            String Dob = cursor.getString(3);
            String emailId = cursor.getString(4);
            String phoneNo = cursor.getString(5);
            String newPassword = cursor.getString(6);

            user.setId(userId);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setDob(Dob);
            user.setEmailId(emailId);
            user.setPhoneNumber(phoneNo);
            user.setPassword(newPassword);
        }
        cursor.close();

        // return contact
        return user;
    }
}
