package com.surebot.myapplication.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferenceManager {

    private SharedPreferences.Editor editor;
    private SharedPreferences pref;

    public SharedPreferenceManager(Context context) {
        pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
    }

    public void save(String key, String value) {
        editor = pref.edit();
        editor.putString(key, value); // Storing string
        editor.apply(); // save data
    }

    public String getValueByKey(String key) {
        return pref.getString(key, null);
    }

    public void clearData() {
        editor = pref.edit();
        editor.clear();
        editor.apply();
    }
}

