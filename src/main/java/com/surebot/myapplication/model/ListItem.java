package com.surebot.myapplication.model;

public class ListItem {
private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
